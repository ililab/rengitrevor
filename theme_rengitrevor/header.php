<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/main.css?v=<?php echo rand() ?>">
    <title>RengiTrevor - <?php the_title(); ?></title>
<link rel="icon" href="https://rengitrevor.com.br/wp-content/uploads/2020/05/favicon.png" type="image/x-icon">
    <?php wp_head(); ?>
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRNM5WD');</script>
<!-- End Google Tag Manager -->
</head>
<body <?php body_class(); ?>>
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WRNM5WD"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <div class="page-wrapper">
		<a href="https://api.whatsapp.com/send?phone=5511941566347" id="whatsapp" class="whats" target="_blank">
<img src="https://rengitrevor.com.br/wp-content/uploads/2020/05/WhatsApp-icone.png">
</a>   
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <a href="<?php echo esc_html( home_url() ); ?>">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/imgs/logo.png" alt="">
                        </a>
                    </div>
                    <div class="col-xs-6 visible-xs">
                        <div class="hambuerger" onclick="myFunction(this)">
                            <div class="bar1"></div>
                            <div class="bar2"></div>
                            <div class="bar3"></div>
                        </div>
                    </div>
                    <div class="col-xs-6 hidden-xs">
                        <p class="contact">
                            <a href="<?php the_field('nosso_contato_link', 'option') ?>" id="nossoContato" style="padding: 20px; border: 1px solid; text-transform: uppercase; color: #f89b25;"><?php the_field('nosso_contato', 'option') ?></a>
                        </p>
                    </div>
                </div>
            </div>
        </header>
        <nav class="menu">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <?php
                            $args = array(
                                'theme_location' => 'main'
                            );

                            wp_nav_menu($args)
                        ?>
                        <!-- <ul>
                            <li><a href="<?php //echo esc_html( home_url() ); ?>/nossa-historia">Nossa história</a></li>
                            <li><a href="<?php //echo esc_html( home_url() ); ?>/o-que-fazemos">O que fazemos</a></li>
                            <li><a href="<?php //echo esc_html( home_url() ); ?>/setores-de-atuacao">Setores de atuação</a></li>
                            <li><a href="<?php //echo esc_html( home_url() ); ?>/no-que-acreditamos">No que acreditamos</a></li>
                            <li><a href="<?php //echo esc_html( home_url() ); ?>/nossos-profissionais">Nossos profissionais</a></li>
                        </ul> -->
                    </div>
                </div>
            </div>
        </nav>
<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Thirteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Odin
 * @since 2.2.0
 */

get_header(); ?>

    <main id="content" class="arquivos" tabindex="-1" role="main">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <?php if ( have_posts() ) : ?>
                        <h1 class="page-title">Blog</h1>
                        <?php while ( have_posts() ) : the_post(); ?>
                            <div class="col-xs-12 col-sm-4">
                                <article id="post-<?php the_ID(); ?>" class="post post-<?php the_ID(); ?>">
                                    <figure>
                                        <a href="<?php echo esc_url( get_permalink() )  ?>">
                                            <img src="<?php echo get_field('imagem_pg_blog')['sizes']['medium']; ?>" alt="" class="img-responsive">
                                        </a>
                                    </figure>
                                    <?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?>
                                    <div class="entry-summary">
                                        <?php the_excerpt(); ?>
                                    </div><!-- .entry-summary -->

                                </article><!-- #post-## -->
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </main>
    <!-- #main -->

<?php get_footer(); ?>

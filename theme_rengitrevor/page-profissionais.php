<?php /* Template Name: Profissionais */ ?>
<?php get_header(); ?>
    <div class="page">
        <section class="nossos-profissionais">
                <div class="container">
                    <div class="row">
                        <h1><?php echo get_field('titulo'); ?></h1>
                    </div>
                    <?php while(have_rows('profissionais')): the_row(); ?>
                    <div class="row">
                        <div class="col-xs-12 col-lg-4">
                            <img src="<?php echo get_sub_field('imagem'); ?>" alt="<?php echo get_sub_field('nome'); ?>">
                        </div>
                        <div class="col-xs-12 col-lg-8">
                            <div class="text">
                                <h2><?php echo get_sub_field('nome'); ?></h2>
                                <?php echo get_sub_field('texto'); ?>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
                </div>
        </section>
    </div>
<?php get_footer(); ?>
<?php 
add_theme_support( 'post-thumbnails' );

register_nav_menus( array( 
    'main' => 'Main Menu', 
    'footer' => 'Footer menu',
    'footer2' => 'Footer menu 2' 
));

if( function_exists( 'acf_add_options_page' ) ) {

    acf_add_options_page(array(
        'page_title'  => 'Opções do Tema',
        'menu_title'  => 'Opções do Tema',
        'menu_slug'   => 'amdt-opcoes',
        'capability'  => 'edit_posts',
        'redirect'    => false
    )); 
}

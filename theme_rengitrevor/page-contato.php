<?php /* Template Name: Contato */ ?>
<?php get_header() ?>
    <section class="contato">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-6">
                <?php echo do_shortcode(get_field('shortcode') ); ?>
                </div>
                <div class="col-xs-12 col-md-5 col-md-offset-1">
                    <h2><?php the_field('titulo'); ?></h2>
                    <p>
                    <?php the_field('informacoes'); ?>
                    </p>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/imgs/mapa.png" alt="" class="img-responsive mapa">
                    
                </div>
            </div>
        </div>
    </section>
<?php get_footer() ?>

<?php /* Template Name: Página com Banner */ ?>
<?php get_header(); ?>
    <div class="page">
        <section class="banner2">
            <img src="<?php the_field('imagem_de_fundo'); ?>" alt="" class="img-responsive">
            <div class="text-display">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-lg-6 col-lg-offset-5">
                            <div class="text">
                                <?php the_field('text'); ?>
                                <!-- <h2>
                                    A Rengi Trevor foi idealizada com <br class="visible-lg">
                                    o objetivo de oferecer serviços <br class="visible-lg">
                                    profissionais com alto padrão de <br class="visible-lg">
                                    qualidade para empresas inovadoras.
                                </h2>
                                <p>
                                    Com mercados cada vez mais competitivos, conhecer seus <br class="visible-lg"> números com precisão, compreender o sistema tributário e <br class="visible-lg"> manter gestão sobre riscos, são fatores essenciais para que <br class="visible-lg"> empreendedores tomem decisões assertivas para o sucesso <br class="visible-lg"> dos negócios. Nossa história é fundamentada em ajudar <br class="visible-lg">  empresas com as melhores soluções em busca de resultados <br class="visible-lg">sólidos e consistentes.
                                </p> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="nossos-profissionais">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <h2>NOSSOS PROFISSIONAIS</h2>
                    </div>
                    <div class="col-xs-12 col-md-4 img">
                        <img src="https://rengitrevor.com.br/wp-content/uploads/2020/06/nelson.png" alt="Nelson">
                    </div>
                    <div class="col-xs-12 col-md-8">
                        <h3>NELSON SOUZA</h3>
                        <p><b>Qualificações</b>
                            <br>
                            Bacharel em Ciências Contábeis.
                        </p>
                        <p>
                            <b>Experiência</b>
                            <br>
                            Atuação em auditoria e consultoria desde 2002 representando grandes empresas
                            internacionais. Experiência em setores como indústria, comércio, incorporação
                            imobiliária, construção e engenharia civil e entidades do terceiro setor.
                            Experiente na condução de projetos de BPS – Business Process Solutions para
                            tornar os processos internos das empresas mais eficientes gerando soluções para
                            elaboração de informações contábeis, folha de pagamento, gestão de tributos e gestão
                            de processos financeiros, potencializando os recursos das organizações para execução
                            realização de suas atividades operacionais.
                        </p>
                        <p>
                            <b>Especialidades</b>
                            <br>
                            Auditoria – Consultoria – Impostos – Compliance Financeiro e Informações Contábeis
                        </p>
                            
                        <p>
                            <b>Sócio Fundador da Rengi Trevor Auditores Independentes</b>
                        </p>
                    </div>
                    <div class="col-xs-12 col-md-4 img">
                        <img src="https://rengitrevor.com.br/wp-content/uploads/2020/06/roberto.png" alt="Roberto">
                    </div>
                    <div class="col-xs-12 col-md-8">
                        <h3>ROBERTO EVANGELISTA</h3>
                        <p>
                            <b>Qualificações</b>
                            <br>
                            Bacharel em Ciências Contábeis e MBA em Gestão de Impostos pela FIPECAFI –
                            Fundação Instituto de Pesquisas Contábeis, Atuariais e Financeiras, Membro do
                            IBRACON – Instituto dos Auditores Independentes do Brasil e Auditor Independente
                            registrado na CVM - Comissão de Valores Imobiliários.
                        </p>
                        <p>
                            <b>Experiência</b>
                            <br>
                            Atuação em auditoria e consultoria desde 1995 representando grandes empresas
                            internacionais. Experiência em setores como indústria, comércio, prestação de
                            serviços, incorporação imobiliária, construção e engenharia civil e entidades
                            do terceiro setor. Participação como sócio líder em processos de fusões e
                            aquisições, investigações de compras e abertura de capital. Experiente na
                            condução de projetos para organização de empresas, aprimorando a gestão de
                            riscos operacionais, apresentação de informações financeiras, planejamento
                            tributário e fortalecimento de controles internos com o objetivo de obter grau
                            de governança corporativa adequado para captação de recursos no mercado de
                            capitais.
                        </p>
                        <p>
                            <b>Especialidades</b>
                            <br>
                            Auditoria – Consultoria – Impostos – Gestão de Riscos e Controles Internos
                        <p>
                            <b>Sócio Fundador da Rengi Trevor Auditores Independentes</b>
                        </p>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php get_footer(); ?>
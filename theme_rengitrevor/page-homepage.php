<?php /* Template Name: Home */ ?>
<?php get_header(); ?>
    <section class="banner">
        <img src="<?php the_field('banner') ?>" alt="" class="img-responsive visible-xs visible-sm">
        <div class="text-display">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text">
                            <h2>
                                <?php the_field('title') ?>
                            </h2>
                            <a href="<?php the_field('link') ?>" id="btnBanner"><?php the_field('botao_conheca_mais') ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img src="<?php the_field('banner') ?>" alt="" class="img-responsive hidden-xs hidden-sm">
    </section>
    <!-- <section class="publicacoes">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2>Publicações</h2>
                </div>
            </div>
            <div class="row">
            <?php// $query =  new WP_Query(
             //   array(
             //   'posts_per_page' => '3',
             //   )
         //   ); ?>
            <?php // if ( $query->have_posts() ) : ?>
            <?php // while ( $query->have_posts() ) : $query->the_post(); ?>

            <div class="col-xs-12 col-md-4">
                <div class="publicacoes-box">
                    <a href="<?php // the_permalink() ?>">
                        <div class="publicacoes-image">
                            <img src="<?php // the_post_thumbnail_url(); ?>" alt="" class="img-responsive">
                        </div>
                    </a>
                        <div class="publicacoes-infos">
                        <a href="<?php // the_permalink() ?>"><h3><?php // the_title(); ?></h3></a>
                        <a href="<?php // the_permalink() ?>"><p><?php // the_excerpt(); ?></p></a>
                        </div>
              
                </div>
            </div>
            <?php // endwhile;    
               // endif;
               // wp_reset_query();
                ?>
                
            </div>
        </div>
    </section> -->
    <section class="servicos">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2><?php the_field('titulo_servicos') ?></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <h3>
                    <?php the_field('texto'); ?>
                    </h3>
                </div>
            </div>
            <div class="row">
            <?php
                if( have_rows('servicos') ):
                    while ( have_rows('servicos') ) : the_row(); ?>
                        <div class="col-xs-12 col-md-3">
                            <div class="box-servicos">
                                <div class="icon">
                                    <img src="<?php the_sub_field('icone'); ?>" alt="">
                                </div>
                                <div class="text">
                                    <p>
                                        <?php the_sub_field('texto'); ?>
                                    </p>
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/imgs/arrow.png" alt="">
                                    <a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('botao'); ?></a>
                                </div>
                            </div>
                        </div>
            <?  endwhile;
                endif; ?>
            </div>
        </div>
    </section>     
    <section class="porque hidden-xs hidden-sm">
        <div class="full">
            <div class="box box1">
                <h2>
                    <?php the_field('titulo_porque') ?>
                </h2>
                <p>
                    <?php the_field('texto-porque') ?>
                </p>
                <a href="<?php the_field('link_porque') ?>"><?php the_field('botao_porque') ?></a>
            </div>
            <div class="box box2">
                <img src="<?php the_field('imagem_por_que') ?>" alt="">
            </div>
        </div>
    </section>
    <section class="porque visible-xs visible-sm">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                        <img src="<?php the_field('imagem_por_que') ?>" alt="" class="img-responsive">
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <h2>
                        <?php the_field('titulo_porque') ?>
                    </h2>
                    <p>
                    <?php the_field('texto-porque') ?>
                    </p>
                    <a href="<?php the_field('link_porque') ?>"><?php the_field('botao_porque') ?></a>
                </div>
            </div>
        </div>
    </section>
    <section class="profissionais">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <img src="<?php the_field('imagem_profissionais') ?>" alt="" class="img-responsive">
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="text">
                        <h2>
                            <?php the_field('titulo_profissionais') ?>
                        </h2>
                        <p>
                            <?php the_field('texto-nosso') ?>
                        </p>
                        <a href="<?php the_field('link_profissionais') ?>"><?php the_field('botao_profissionais') ?></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>
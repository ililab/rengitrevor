<?php /* Template Name: Página o que fazemos */ ?>
<?php get_header(); ?>
    <div class="page">
        <section class="banner2">
            <img src="<?php the_field('imagem_de_fundo'); ?>" alt="" class="img-responsive">
            <div class="text-display">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-lg-6 col-lg-offset-5">
                            <div class="text">
                                <?php the_field('text'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <section class="oque-fazemos">
        <div class="container">
            <div class="row">
                <img src="<?php the_field('imagem_1'); ?>" alt="" class="img-responsive hidden-lg hidden-md">
                <div class="col-xs-12 col-md-6">
                    <div class="texto-image">
                        <?php the_field('titulo_1'); ?>
                        <?php the_field('texto_1'); ?>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <img src="<?php the_field('imagem_1'); ?>" alt="" class="img-responsive visible-lg visible-md m-l">
                </div>
            </div>
            <div class="row">
                <img src="<?php the_field('imagem_2'); ?>" alt="" class="img-responsive hidden-lg hidden-md">
                <div class="col-xs-12 col-md-6">
                    <img src="<?php the_field('imagem_2'); ?>" alt="" class="img-responsive visible-lg visible-md m-r">
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="texto-image">
                        <?php the_field('titulo_2'); ?>
                        <?php the_field('texto_2'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <img src="<?php the_field('imagem_3'); ?>" alt="" class="img-responsive hidden-lg hidden-md  ">
                <div class="col-xs-12 col-md-6">
                    <div class="texto-image">
                        <?php the_field('titulo_3'); ?>
                        <?php the_field('texto_3'); ?>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <img src="<?php the_field('imagem_3'); ?>" alt="" class="img-responsive visible-lg visible-md m-l">
                </div>
            </div>
            <div class="row">
                <img src="<?php the_field('imagem_4'); ?>" alt="" class="img-responsive hidden-lg hidden-md">
                <div class="col-xs-12 col-md-6">
                    <img src="<?php the_field('imagem_4'); ?>" alt="" class="img-responsive visible-lg visible-md  m-r">
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="texto-image">
                        <?php the_field('titulo_4'); ?>
                        <?php the_field('texto_4'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>
     
    <footer>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/imgs/logo.png" alt="">
                    </div>
                    <div class="col-xs-12 col-md-3">
                        <?php
                            $args = array(
                                'theme_location' => 'footer'
                            );

                            wp_nav_menu($args)
                        ?>
                        <!-- <ul>
                            <li><a href="<?php //echo esc_html( home_url() ); ?>/nossa-historia">Nossa história</a></li>
                            <li><a href="<?php //echo esc_html( home_url() ); ?>/o-que-fazemos">O que fazemos</a></li>
                            <li><a href="<?php //echo esc_html( home_url() ); ?>/setores-de-atuacao">Setores de atuação</a></li>
                        </ul> -->
                    </div>
                    <div class="col-xs-12 col-md-3">
                        <?php
                            $args = array(
                                'theme_location' => 'footer2'
                            );

                            wp_nav_menu($args)
                        ?>
                        <!-- <ul>
                            <li><a href="<?php //echo esc_html( home_url() ); ?>/no-que-acreditamos">No que acreditamos</a> </li>
                            <li><a href="<?php //echo esc_html( home_url() ); ?>/nossos-profissionais">Nossos profissionais</a></li>
                            <li><a href="<?php //echo esc_html( home_url() ); ?>/contato">Contato</a></li>
                        </ul> -->
                    </div>
                </div>
                <div class="row copyright">
                    <div class="col-xs-12">
                        <p>
                            <!-- © Copyright 2020 -->
                            <a href="https://agenciaili.com.br/" target="_blank">
                                <small>
                                    Designed by</small>
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/imgs/ili.png" width="20"
                                    alt="Agência Ili - Marketing Digital: SEO, Links Patrocinados, Facebook Ads e outros"
                                    title="Agência Ili - Marketing Digital: SEO, Links Patrocinados, Facebook Ads e outros">
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        function myFunction(x) {
            x.classList.toggle("change");
        }
        $( ".hambuerger" ).click( function() {
            $( 'nav.menu' ).toggleClass('active');
        });
    </script>
    <?php wp_footer(); ?>
</body>
</html>
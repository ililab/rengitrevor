<?php get_header() ?>
    <section class="single">
        <div class="container">
            <div class="row">
                <div class="banner">
                    <img src="<?php the_post_thumbnail_url(); ?>" alt="" class="img-responsive">
                </div>
                <div class="col-xs-12 col-md-10 col-md-offset-1">
                    <?php while ( have_posts() ) : the_post(); ?>
                        <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
                        <?php the_content(); ?>
                   <?php endwhile; ?>
                </div>
            </div>
        </div>
    </section>
<?php get_footer() ?>

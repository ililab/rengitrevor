<?php /* Template Name: Página com Banner */ ?>
<?php get_header(); ?>
    <div class="page">
        <section class="banner2">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/imgs/bg-indeax2-new.png" alt="" class="img-responsive">
            <div class="text-display">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-lg-6 col-lg-offset-5">
                            <div class="text">
                                <h2>
                                    A Rengi Trevor foi idealizada com <br class="visible-lg">
                                    o objetivo de oferecer serviços <br class="visible-lg">
                                    profissionais com alto padrão de <br class="visible-lg">
                                    qualidade para empresas inovadoras.
                                </h2>
                                <p>
                                    Com mercados cada vez mais competitivos, conhecer seus <br class="visible-lg"> números com precisão, compreender o sistema tributário e <br class="visible-lg"> manter gestão sobre riscos, são fatores essenciais para que <br class="visible-lg"> empreendedores tomem decisões assertivas para o sucesso <br class="visible-lg"> dos negócios. Nossa história é fundamentada em ajudar <br class="visible-lg">  empresas com as melhores soluções em busca de resultados <br class="visible-lg">sólidos e consistentes.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php get_footer(); ?>